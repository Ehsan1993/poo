package exercice1_1Calculatrice;

public class Calculator {

	static double result = 0;

	public static double result(char operator, double number1, double number2) {
		
		switch (operator) {
		case '-':
			result = number1 - number2;
			break;
		case '+':
			result = number1 + number2;
			break;
		case '*':
			result = number1 * number2;
			break;
		case '/':
			result = number1 / number2;
		}
		System.out.println(result);
		return result;
	}
}	