package exercice1_3PersonneAdresse;

import exercice1_3PersonneAdresse.Outils;
import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {

		var adresse1 = new Adresse(2, "rue Victor Hugo", 75008, "Paris");
		var adresse2 = new Adresse(17, "rue de la République", 44000, "Nantes");
		var adresse3 = new Adresse(55, "Bld de la Libération", 59000, "Lille");

		var jean = new Personne("Jean", "Dupont", 30, adresse3);
		var bernard = new Personne("Bernard", "Morin", 45, adresse1);
		var nathalie = new Personne("Nathalie", "Durand", 33, adresse2);

		System.out.println(jean);
		System.out.println(bernard);
		System.out.println(nathalie);

		// 4ème étape, affichage des adresses
		
		adresse1.Afficher();
		adresse2.Afficher();
		adresse3.Afficher();

		// 9ème étape, affichage des personnes avec leurs adresses
		
		jean.Afficher();
		bernard.Afficher();
		nathalie.Afficher();
		
		// 10ème étape, changement d'adresse de Jean
		
		jean.setAdresse(new Adresse(44, "rue des Docks", 33000, "Bordeaux"));
		jean.Afficher();

		// 11ème étape, marriage de Bernard et Natahlie et changement d'adresse de Nathalie
		
		nathalie.setAdresse(new Adresse(2, "rue Victor Hugo", 75008, "Paris"));
		JOptionPane.showMessageDialog(null, nathalie.toString() + "\n" + bernard.toString(), "Vive les mariés!!", JOptionPane.NO_OPTION);
		
		// 12, 13 et 14 ème étaps, Utilitaire
		
		Outils.afficher("Bernard", bernard);
	}

}
