package exercice1_3PersonneAdresse;

import javax.swing.JOptionPane;

public class Personne {

	private String prenom, nom;
	private int age;
	private Adresse adresse;

	Personne(String prenom, String nom, int age, Adresse adresse){
		this.prenom = prenom;
		this.nom = nom.toUpperCase();
		this.age = age;
		this.adresse = adresse;
	}

	public String getPrenom() {
		return prenom;
	}
	public String getNom() {
		return nom;
	}
	public int getAge() {
		return age;
	}
	public Adresse getAdresse() {
		return adresse;
	}
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String toString() {
		return prenom + " " + nom + " " + age + " ans, habite au " + adresse;
	}
	public void Afficher() {
		JOptionPane.showMessageDialog(null, prenom + " " + nom +" "+ age + " ans, habite au " + adresse, prenom ,JOptionPane.NO_OPTION);
	}
}