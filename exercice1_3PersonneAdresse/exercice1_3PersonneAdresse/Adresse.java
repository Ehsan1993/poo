package exercice1_3PersonneAdresse;

import javax.swing.JOptionPane;

public class Adresse {

	int numeroRue;
	String rue;
	int codePostal;
	String ville;

	Adresse(int numeroRue, String rue, int codePostal, String ville){
		this.numeroRue = numeroRue;
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;			
	}

	public String toString() {
		return numeroRue + ", " + rue + ", " + codePostal + ", " + ville;
	}
	public void Afficher() {
		JOptionPane.showMessageDialog(null, "Adresse: " + numeroRue + " " + rue + ", " + codePostal + " " + ville, "Adresse à " + ville, JOptionPane.NO_OPTION);
	}
}
