package com.ehsan.entities;

public class Personne {

	private String name;
	private String societe;
	private static final String PAS_DE_SOCIETE = "?";

	public Personne(String name, String societe){
		this.name = name.toUpperCase();
		this.societe = validerSociete(societe).toUpperCase();
	}

	public Personne(String name){
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getSociete() {
		return societe;
	}

	public void setSociete(String societe) {
		if (this.getSociete() != PAS_DE_SOCIETE){
			System.out.println("Error:\n1- Quitter la societe, puis\n2- Affecter Societe");
			System.exit(0);
		}
		this.societe = validerSociete(societe);
	}

	public void quitSociete() {
		if (this.societe == null) {
			afficher();
			System.out.println("Je ne suis pas salarié: impossible de quitter la société");
			System.exit(1);
		}
		else {
			this.societe = PAS_DE_SOCIETE;
		}
	}

	private String validerSociete(String societe) {
		if (societe == PAS_DE_SOCIETE || societe.length() >= 30) {
			System.out.println("The societe is not correct!!!");
			System.exit(2);
		}

		return societe;

	}

	public void afficher(){

		if (societe == PAS_DE_SOCIETE || societe == null) {
			System.out.println("My name is " + name + " and I am not working anymore");
		}
		else {
			System.out.println("My name is " + name + " and I am working in " + societe); 
		}
	}

}
