package com.ehsan.application;

import com.ehsan.entities.Personne;

public class Principle {

	public static void main(String[] args) {

		// jusqu'au 11ème étape, afficher le passage dans main

		System.out.println("Passage dans main");

		// 12ème étape, 

		var Martin = new Personne("Martin", "Java SARL");
		var Dupont = new Personne("Dupont");

		//13ème étape, afficher

		Martin.afficher();
		Dupont.afficher();

		//14ème étape, afficher quitterSociete

		//	Dupont.quitSociety();

		Personne Durand = new Personne ("Durant", "J2E SA");
		Durand.afficher();
		Durand.quitSociete();
		
		Durand.afficher();

		// 15 et 16ème étapes, changer l'entreprise par setter

		Durand.setSociete("EJB Corporate");
		Durand.afficher();
		
		// 17 et 18ème étapes,
		
//		Personne Mike = new Personne("Mike", "?");
//		Mike.afficher();

		Durand.setSociete("EJB Corporate");
		Durand.afficher();
		
	}

}
