package exercice1_4;

import javax.swing.JOptionPane;
import com.ehsan.types.CategorieUtilisateur;
import com.ehsan.types.Utilisateur;

public class Main {

	public static void main(String[] args) {

		//étape 4:
		Forum forum = new Forum();
		Utilisateur jean = new Utilisateur("Jean", "Dupont", CategorieUtilisateur.STANDARD);
		Utilisateur bernard = new Utilisateur("Bernard", "Morin", CategorieUtilisateur.STANDARD);
		Utilisateur nathalie = new Utilisateur("Nathalie", "Morin", CategorieUtilisateur.MODERATEUR);
		
		JOptionPane.showMessageDialog(null, jean, "jean", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, bernard, "bernard", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, nathalie, "nathalie", JOptionPane.INFORMATION_MESSAGE);
		
		//étape 10:
		
		Message message1 = new Message("Java SE", "L'encapsulation est une propriété fondamentale", jean, forum);
		Message message2 = new Message("Java SE1", "L'encapsulation est une propriété fondamentale - 1", bernard, forum);
		//étape 11:
		
		JOptionPane.showMessageDialog(null, message1.toString());
		forum.getListeMessagesAuteur();
		
	}

}
