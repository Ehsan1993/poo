package exercice1_4;

import javax.swing.JOptionPane;

public class Forum {

	final int NBR_MESSAGES = 40;
	int compteurMessage = 0;
	MessageUtilisateur message[] = new MessageUtilisateur [NBR_MESSAGES];
	
	public Forum(){
		
	}
	
	public void ajouterMessage (MessageUtilisateur message) {
		this.message[compteurMessage] = message;
		this.compteurMessage ++;
	}
	
	
	public String ajouterListeMessagesAuteur ()
	{
		String list = "";
		for (int i = 0; i < this.compteurMessage; i++) {
			list = list + this.message[i].toString();
		}
		return list;
	}
	
	public void getListeMessagesAuteur() {
		JOptionPane.showMessageDialog(null, 
				this.ajouterListeMessagesAuteur(), "Messages", JOptionPane.INFORMATION_MESSAGE);
	}
}