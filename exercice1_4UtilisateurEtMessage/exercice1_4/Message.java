package exercice1_4;

import java.text.DateFormat;
// étape 6:
import java.util.Date;

import com.ehsan.types.Utilisateur;

public class Message {

	//étape 5:
	
	private String titre;
	private String texte;
	private Date dateCreation;
	private Utilisateur utilisateur;

	//étape 7:
	
	public Message(String titre, String texte, Utilisateur utilisateur, Forum forum) {
		this.setTitre(titre);
		this.setTexte(texte);
		this.utilisateur = utilisateur;
		this.dateCreation = new Date();
		MessageUtilisateur msg = new MessageUtilisateur(this);
		msg.ajouterForumMessages(forum);
	}
	
	//étape 9:
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getTexte() {
		return texte;
	}
	public void setTexte(String texte) {
		this.texte = texte;
	}

	public String getDateCreation() {
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
		return df.format(dateCreation);
	}
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}
	//étape16:
	public String getAuteur() {
		return utilisateur.getPrenom() + " " + utilisateur.getNom() + ", " + utilisateur.getStatut();
	}

	//étape 8:
	
	@Override
	public String toString() {
		return titre + "--" + texte + "\nDate de creation: " + getDateCreation() + "\n" + utilisateur;
	}

}
