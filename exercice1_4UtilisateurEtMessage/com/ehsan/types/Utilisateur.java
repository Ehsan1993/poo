package com.ehsan.types;

public class Utilisateur {
	
	// étape 2:
	
	private String prenom;
	private String nom;
	private CategorieUtilisateur statut;
	
	public Utilisateur(String prenom, String nom, CategorieUtilisateur statut){
		this.prenom = prenom;
		this.nom = nom;
		this.statut = statut;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public String getNom() {
		return nom;
	}
	public CategorieUtilisateur getStatut() {
		return statut;
	}
	
	// étape 3:
	
	@Override
	public String toString() {
		return "Utilisateur: "+prenom+" "+nom+", "+statut;
	}
}
